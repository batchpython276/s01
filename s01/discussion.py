# [Section] Comments
# Comments in Python are done using "ctrl+/" or # symbol

# [Section] Python Syntax
# Hello World in Python


print("Hello World!");

# [Section] Indentation
# where in other progamming languages the indentation in code is for readability only, the indentation in Python is very important.
# In Python, indentation is used to indicate a block of code

# Similar to JS, there is no need to end statements with semicolons


# [Section] Variables
# Variable are the container of data
# In Python, a variable is declared by stating the variable name and assigning a value using the equality symbol

# [Section] Naming Convention
# The terminology used for variable names is identifier
# All identifiers should begin with a letter (A to Z or a to z), dollar sign or an underscore.
# after the first char, identifier can have any combination of characters
# Unlike Javascript that uses the camel casing, Python uses the snake case convention for variable as defined in the PEP (Pyhton enhancement proposal).
# Most importantle, identifiers are case sensitive.

age = 35
mid_int = "C"

print(mid_int)
print(age)

name1, name2, name3, name4 = "John", "Paul", "George", "Ringo"

print(name2)

# [Section] Data Types
# Data types conveys what kind of information a variable holds. there are diff data types and each has its own use.


# in python, there are the commonly used data types:
# 1. String(str) - for alphanumeric and symbols
full_name = "John Doe"
secret_code = "Pa$$word"

# 2. Numbers(int, float, complex) - for integers, decimal and complex numbers
num_of_days = 365 # this is an integer
pi_approx = 3.1416 # this is a float
complex_num = 1 + 5j # this is a complex number


print(type(full_name))


# 3. Boolen(bool) - for truth values
# Boolen values in Pyhton starts with uppercase letter

isLearning = True
isDifficult = False

# [Section] Using Variables
# Just like in JS variables are used by simply calling the name of the identifier

print("My name is " + full_name)
print(full_name + " " + secret_code)


# [Section] Terminal Outputs
# in python, printing in the terminal uses the print() function
# to use variables, concatenate (+ symbol) between strings that can be used
# However we cannot concatenate str to a number or to a different data type

# print("My age is " + age)

# [Section] Typecasting
# functions that can be used in typecastings
# 1. int() - convert to integer
print(int(3.15))
# 2. float() - convert to float
print(float(5))
# 3. str() - conver to string
print("My age is " + str(age))

# another way to avoid type error in printing w/o the use of typecasting
# f-strings
print(f"Hi my name is {full_name} and my age is {age}.")


# [Section] Operations
# Python has operator families that can be used to manipulate variables

# Arithmetic Operator - performs mathematical operations

print(1+10)
print(15-8)
print(18*9)
print(21/7)
print(18%4)
print(2**6)


# Assignment operators - used to assign to variables

num1 = 4
num1 += 3
print(num1)

# other operators -=, *=, /=, %=


# Comparison operators - used compare values (boolean values)

print(1 == "1")

# other operators, !=, >=, <=, >, <


# Logical operators - used to combine conditional statements

# Logical or, and, not operator
print(True or False or True and False)
print(True and False)
print(not False)

# https://gitlab.com/tuitt/students/batch276/resources/s01/discussion